import React from "react";
import styled from "styled-components";
import Cookies from "js-cookie";
export default function Header() {
  return (
    <TitleContainer>
      <Title>Weather App</Title>
      {Cookies.get("user") ? (
        <LogOut
          onClick={() => {
            Cookies.set("user", "");
            window.location.reload();
          }}
        >
          Log Out
        </LogOut>
      ) : (
        <></>
      )}
    </TitleContainer>
  );
}
const TitleContainer = styled.div`
  width: 100%;
  height: 70px;
  display: flex;
  flex-direction: row;
  align-self: flex-start;
  justify-content: space-between;
  @media only screen and (max-width: 1366px) {
    width: 1340px;
  }
  @media only screen and (max-width: 1280px) {
    width: 1250px;
  }
  @media only screen and (max-width: 1024px) {
    width: 990px;
  }
  @media only screen and (max-width: 800px) {
    width: 770px;
  }
  @media only screen and (max-width: 600px) {
    width: 550px;
  }
  @media only screen and (max-width: 420px) {
    width: 390px;
  }
  @media only screen and (max-width: 380px) {
    width: 350px;
  }
  @media only screen and (max-width: 320px) {
    width: 300px;
  }
`;
const Title = styled.h1`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  line-height: 47px;
  margin: 30px;
  @media only screen and (max-width: 800px) {
    font-size:30px;
  }
   @media only screen and (max-width: 420px) {
     font-size:25px;
  }
 
`;
const LogOut = styled.button`
  width: 80px;
  height: 30px;
  align-self: flex-end;
  background: #000000;
  border: 1px solid #000000;
  box-sizing: border-box;
  color: #ffffff;
  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  margin-top: 40px;
`;
