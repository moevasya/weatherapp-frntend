import React from "react";
import Header from "./Header";
import styled from "styled-components";
import Footer from "./Footer";
import { Link } from "react-router-dom";
export default function PageLoader() {
  return (
    <Container>
      <Header />
      <p className="escape">404 PAGE NOT FOUND</p>
      <p className="escape">
        Looks Like we ran into a problem make sure u entered a valid city name
      </p>
      <Link className="escape" to="/">
        <p className="escape">Take me back home</p>{" "}
      </Link>
      <Footer />
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 1400px;
  justify-content: space-between;
`;
