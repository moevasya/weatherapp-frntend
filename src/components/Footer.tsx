import React from 'react'
import styled from 'styled-components'
export default function Footer() {
    return (
     
        <FooterText>@ Weather 2021, All rights reserved</FooterText>
      
    );
}
const FooterText = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  align-self: flex-start;
`;
