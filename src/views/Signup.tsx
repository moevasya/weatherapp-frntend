import React from "react";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import Header from "../components/Header";
import axios from "axios";
import Footer from "../components/Footer";

export default function LogIn() {
  const history = useHistory();

  let info = {
    username: "",
    password: "",
  };

  const changeUserName = (event: any) => {
    info.username = event.target.value;
  };
  const changePassword = (event: any) => {
    info.password = event.target.value;
  };

  const onSubmit = (event: any) => {
    event.preventDefault();
    const registered = {
      username: info.username,
      password: info.password,
    };
    axios
      .post("http://localhost:5000/app/signup", registered)
      .then((response) => {
        if (response.data === 200) {
          info.username = "";
          info.password = "";
          history.push("/");
        } else if (response.data === "not signed up") {
          alert(
            "sign up failed please make sure all fields are filled or try a different username",
          );
        }
      });
  };
  return (
    <MainContainer>
      <Header />
      <MidContainer onSubmit={onSubmit}>
        <SignUpLabel>Sign Up</SignUpLabel>
        <Username
          type="text"
          placeholder="Username"
          onChange={changeUserName}
        ></Username>
        <Password
          type="password"
          placeholder="Password"
          onChange={changePassword}
        ></Password>
        <Button type="submit" value="SignUp" onClick={onSubmit}>
          Sign Up
        </Button>
        <Link to="/">
          <LogInLabel className="Links">
            Already Have An Account? Log In !{" "}
          </LogInLabel>
        </Link>
      </MidContainer>
      <Footer />
    </MainContainer>
  );
}
const MainContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const MidContainer = styled.form`
  width: 500px;
  height: 400px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media only screen and (max-width: 420px) {
    width: 400px;
  }
  @media only screen and (max-width: 375px) {
    width: 350px;
  }
  @media only screen and (max-width: 320px) {
    width: 300px;
    margin-left: -55px;
  }
`;
const SignUpLabel = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  line-height: 47px;

  align-self: flex-start;
  @media only screen and (max-width: 420px) {
    font-size: 30px;
  }
`;
const Username = styled.input`
  width: 497px;
  height: 54px;
  background: #ffffff;
  border: 1px solid #000000;
  box-sizing: border-box;
  height: 50px;
  padding: 15px;
  @media only screen and (max-width: 420px) {
    width: 395px;
  }
  @media only screen and (max-width: 375px) {
    width: 310px;
    height: 40px;
  }
  @media only screen and (max-width: 320px) {
    width: 250px;
  }
`;
const Password = styled.input`
  width: 497px;
  height: 54px;
  background: #ffffff;
  border: 1px solid #000000;
  box-sizing: border-box;
  padding: 15px;
  @media only screen and (max-width: 420px) {
    width: 395px;
  }
  @media only screen and (max-width: 375px) {
    width: 310px;
    height: 40px;
  }
  @media only screen and (max-width: 320px) {
    width: 250px;
  }
`;
const Button = styled.button`
  width: 136px;
  height: 54px;
  align-self: flex-end;
  background: #000000;
  border: 1px solid #000000;
  box-sizing: border-box;
  color: #ffffff;
  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  @media only screen and (max-width: 420px) {
    width: 125px;
    margin-right: 5px;
  }
  @media only screen and (max-width: 375px) {
    margin-right: 40px;
    height: 40px;
  }
  @media only screen and (max-width: 320px) {
    margin-right: 53px;
    width: 70px;
  }
`;
const LogInLabel = styled.a`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  line-height: 47px;
  width: 230px;
  height: 47px;
  color: black;
  text-decoration: none;
  @media only screen and (max-width: 420px) {
    font-size: 25px;
  }
`;
