import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Header from "../components/Header";
import Footer from "../components/Footer";
import axios from "axios";
import Clock from "react-live-clock";
import Loading from "../components/PageLoader";
import rain from "../images/rain.svg";
import drizzle from "../images/drizzle.svg";
import fog from "../images/fog.svg";
import clouds from "../images/cloud.svg";
import snow from "../images/snowflake.svg";
import storm from "../images/storm.svg";
import sun from "../images/sun.svg";

export default function Home() {
  let cityName: string;
  const [city, setCity] = useState("amman");
  const [Icon, setIcon] = useState("");
  const changeC = document.getElementById("C");

  const changeF = document.getElementById("F");
  const [triggerEffect, setTriggerEffect] = useState(0);
  const textChange = (event: any) => {
    cityName = event.target.value;
  };

  const [weatherData, setWeatherData] = useState(Object);

  const time = Date();
  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(`http://localhost:5000/app/weather?address=${city}`)
        .then((response) => {
          setWeatherData(response.data);
          setIcon(response.data.weather["0"].icon);
        });
    };
    fetchData();
  }, [triggerEffect]);
  const [celsius, setCelsius] = useState(true);
  const onSubmitText = (event: any) => {
    event.preventDefault();
    setTriggerEffect(triggerEffect + 1);
    if (cityName) {
      setCity(cityName);
    }
  };
  function convertCels() {
    setCelsius(true);
    console.log(changeF + " " + changeC);
    if (changeC && changeF) {
      changeC.style.color = "#0000ff";
      changeF.style.color = "000000";
    }
  }
  function convertFeh() {
    setCelsius(false);
    console.log(changeF + " " + changeC);
    if (changeC && changeF) {
      changeC.style.color = "#000000";
      changeF.style.color = "0000ff";
    }
  }
  useEffect(() => {
    switch (Icon.substr(0, 2)) {
      case "01":
        setIcon(sun);
        break;
      case "50":
        setIcon(fog);
        break;
      case "09":
        setIcon(drizzle);
        break;
      case "11":
        setIcon(storm);
        break;
      case "09":
        setIcon(rain);
        break;
      case "10":
        setIcon(rain);
        break;
      case "13":
        setIcon(snow);
        break;
      case "02":
        setIcon(clouds);
        break;
      case "03":
        setIcon(clouds);
        break;
      case "04":
        setIcon(clouds);
        break;
    }
  }, [Icon]);
  return weatherData["name"] && Icon ? (
    <MainContainer>
      <Header />
      <SearchBoxDiv>
        <Button type="submit" onClick={onSubmitText}></Button>
        <SearchBox
          id="text"
          type="text"
          placeholder="Location"
          onChange={textChange}
        ></SearchBox>
      </SearchBoxDiv>
      <MainMidContainer>
        <SecMidContainer>
          <Vector src={Icon} />
        </SecMidContainer>
        <SecMidContainer>
          <CityName>
            {weatherData["name"]},{weatherData.sys["country"]}
          </CityName>
          <Time>
            {time.toString().substr(0, 3) + " "}
            <Clock format={"HH:mm"} ticking={true} timezone={"Asia/Amman"} />
          </Time>
          <Condition>{weatherData.weather["0"].description}</Condition>
          <Temp id="temp">
            {celsius
              ? parseInt(weatherData.main["temp"]) - 270 + " C"
              : parseInt(
                  parseInt(weatherData.main["temp"]) * (9 / 5) - 459 + "",
                ) + " F"}
            {"   "}
            <SuperScriptC
              onClick={() => {
                convertCels();
              }}
            >
              C
            </SuperScriptC>
            {"   "}
            <SuperScriptF
              id="F"
              onClick={() => {
                convertFeh();
              }}
            >
              F
            </SuperScriptF>
          </Temp>
        </SecMidContainer>
      </MainMidContainer>
      <Footer />
    </MainContainer>
  ) : (
    <Loading />
  );
}
const MainContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const SearchBoxDiv = styled.form`
  width: 500px;
  height: 55px;
  display: flex;
  align-self: center;
  @media only screen and (max-width: 800px) {
    margin-left: -5%;
  }
  @media only screen and (max-width: 600px) {
    width: 450px;
    margin-left: -12%;
  }
  @media only screen and (max-width: 420px) {
    width: 370px;
  }
  @media only screen and (max-width: 375px) {
    width: 320px;
  }
`;
const SearchBox = styled.input`
  width: 100%;
  height: 54px;
  background: #ffffff;
  border: 1px solid #000000;
  box-sizing: border-box;
  padding: 15px;
  @media only screen and (max-width: 375px) {
    height: 40px;
  }
`;

const MainMidContainer = styled.div`
  width: 80%;
  heigh: 330px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-self: center;
  @media only screen and (max-width: 420px) {
    width: 100%;
    flex-direction: column;
    justify-content: flex-start;
  }
  @media only screen and (max-width: 375px) {
    width: 80%;
    justify-content: flex-start;
  }
`;

const SecMidContainer = styled.div`
  width: 40%;
  height: 330px;
  display: flex;
  flex-direction: column;
  @media only screen and (max-width: 420px) {
    width: 40%;
    height: 150px;
    align-self: center;
  }
  @media only screen and (max-width: 375px) {
    width: 40%;
    height: 100px;
    justify-content: flex-start;
  }
`;
const Vector = styled.img`
  height: 330px;
  width: 330px;
  @media only screen and (max-width: 800px) {
    margin-left: -5%;
    height: 300px;
    width: 300px;
  }
  @media only screen and (max-width: 420px) {
    margin-left: -5%;
    height: 150px;
    width: 150px;
  }
  @media only screen and (max-width: 375px) {
    height: 80px;
    width: 80px;
  }
`;
const CityName = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: 900;
  font-size: 40px;
  line-height: auto;
  @media only screen and (max-width: 800px) {
    font-size: 35px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 25px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 20px;
  }
`;
const Time = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: auto;
  @media only screen and (max-width: 800px) {
    font-size: 35px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 25px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 20px;
  }
`;
const Condition = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: auto;
  @media only screen and (max-width: 800px) {
    font-size: 25px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 15px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 10px;
  }
`;

const Temp = styled.p`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 100px;
  line-height: auto;
  margin-top: 15 %;
  @media only screen and (max-width: 800px) {
    font-size: 80px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 50px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 30px;
  }
`;
const SuperScriptC = styled.sup`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 47px;
  @media only screen and (max-width: 800px) {
    font-size: 35px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 25px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 15px;
  }
`;
const SuperScriptF = styled.sup`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 47px;
  @media only screen and (max-width: 800px) {
    font-size: 35px;
  }
  @media only screen and (max-width: 420px) {
    font-size: 25px;
  }
  @media only screen and (max-width: 375px) {
    font-size: 15px;
  }
`;
const Button = styled.input`
  background-color: transparent;
  color: transparent;
  border: none;
`;
