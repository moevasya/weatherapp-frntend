import React, { useState } from "react";
import "./App.scss";
import LogIn from "./views/Login";
import SignUp from "./views/Signup";
import { Switch, BrowserRouter, Route, Redirect } from "react-router-dom";
import Home from "./views/Home";
import Cookies from "js-cookie";

function App() {
  const [auth, setAuth] = useState(false);
  console.log(Cookies.get("user"));

  const ProtectedRoute = ({ component: Component, ...rest }: any) => {
    return (
      <Route
        {...rest}
        render={() =>
          auth || Cookies.get("user") ? <Component /> : <Redirect to="/" />
        }
      />
    );
  };

  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            {Cookies.get("user") ? (
              <Redirect to="/Home" />
            ) : (
              <LogIn auth={auth} setAuth={setAuth} />
            )}
          </Route>
          <Route path="/Signup" component={SignUp} />

          <ProtectedRoute component={Home} path="/Home" />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
